unit UParserTestMain;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls;

type
  TForm2 = class(TForm)
    eExpression: TEdit;
    m1: TMemo;
    b1: TButton;
    procedure b1Click(Sender: TObject);
  private
    { Private declarations }
  public
    function OnGetValue(Sender:TObject; const AVarName:string): Single;
  end;

var
  Form2: TForm2;

implementation

uses
  UVK_ExpressionParser;

{$R *.dfm}

procedure TForm2.b1Click(Sender: TObject);
var
  p:TVK_ExpressionParser;
  i: Integer;
begin
  p:=TVK_ExpressionParser.Create(eExpression.Text, OnGetValue);
  try
    p.Parse();
    m1.Lines.Add( p.toString( p.ResultList ) );
    m1.Lines.Add( FormatFloat('0.00', p.Evaluate() ) );
  finally
    p.Free;
  end;
end;

function TForm2.OnGetValue(Sender:TObject; const AVarName: string): Single;
begin
  if AnsiSameText(AVarName,'Demo') then
    result := 18
  else
  if AnsiSameText(AVarName,'Var2') then
    result := 3.5;
  m1.Lines.Add(Format('Requested variable %s. Value %s',[AVarName, FormatFloat('0.00',result)]));
end;

end.
