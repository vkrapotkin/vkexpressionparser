program ParserTest;

uses
  Vcl.Forms,
  UParserTestMain in 'UParserTestMain.pas' {Form2},
  UVK_ExpressionParser in '..\UVK_ExpressionParser.pas';

{$R *.res}

begin
  ReportMemoryLeaksOnShutdown := true;
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TForm2, Form2);
  Application.Run;
end.
