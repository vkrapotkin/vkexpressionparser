unit UVK_ExpressionParser;

interface
uses System.Types, System.classes, System.sysutils, System.Generics.Collections;

type
  TVK_EP_ElementType=(etOperand,etOperator,etOpenBracket,etCloseBracket);
  TVK_EP_OnGetValue=function (Sender:TObject; const AVarName:string): Single of object;
  TVK_EP_Element=class
  public
    text:string;
    kind: TVK_EP_ElementType;
    priority:integer;
    value: Single;
    isExpression:Boolean;

    procedure AssignFrom(const e:TVK_EP_Element);
    procedure CheckExpression();
    constructor Create(const AText: string); overload;
    constructor Create(AValue:Single); overload;
    destructor Destroy; override;
    constructor Clone(const e:TVK_EP_Element);
  end;

  TVK_EP_Elements = class(TObjectList<TVK_EP_Element>)
  public
    function AddNew(const AText:string): TVK_EP_Element;
    constructor Clone(const E:TVK_EP_Elements);
  end;

  TVK_ExpressionParser=class
  private
    FOnGetValue: TVK_EP_OnGetValue;
    procedure CheckBrackets;
    procedure Split;
//    function FindSubExpression(var ind1, ind2: integer): Boolean;
    procedure CheckSyntax;
    procedure ConvertToRPN();
    procedure SetOnGetValue(const Value: TVK_EP_OnGetValue);
    function GetValue(e: TVK_EP_Element): single;
  public
    SrcQueue:TVK_EP_Elements;
    ResultList:TVK_EP_Elements;
    Expression:string;

    function ToString(Q: TVK_EP_Elements): string; reintroduce; overload;
    function ToString(q: TStack<TVK_EP_Element>):string; reintroduce; overload;

    function Parse():Boolean;
    function Evaluate():Single;
    constructor Create(const AExpression: string; AGetValueFunction:TVK_EP_OnGetValue);
    destructor Destroy; override;

    property OnGetValue: TVK_EP_OnGetValue read FOnGetValue write SetOnGetValue;
  end;


implementation

var
  localFS:TFormatSettings;

{ TVK_ExpressionParser }

constructor TVK_ExpressionParser.Create(const AExpression: string; AGetValueFunction:TVK_EP_OnGetValue);
begin
  inherited Create();
  Expression := AExpression;
  ResultList := TVK_EP_Elements.Create();
  FOnGetValue := AGetValueFunction;
  SrcQueue:=TVK_EP_Elements.Create;
end;

destructor TVK_ExpressionParser.Destroy;
begin
  FreeAndNil(SrcQueue);
  FreeAndNil(ResultList);
  inherited Destroy;
end;

procedure TVK_ExpressionParser.SetOnGetValue(const Value: TVK_EP_OnGetValue);
begin
  FOnGetValue := Value;
end;

procedure TVK_ExpressionParser.Split();
var
  i,p:integer;
  List:TStringList;
const
  separators='+-*/()';
begin
  List:=TStringList.Create;
  try
    p := 0;
    i := 0;
    while i<Expression.Length do
    begin
      if separators.Contains(Expression.Chars[i]) then
      begin
        // token
        if p<>i then
          List.Add(Trim(Expression.Substring(p,i-p)));
        // separator
        List.Add(Expression.Substring(i,1));
        p:=i+1;
      end;
      inc(i);
    end;
    if p<>i then
      List.Add(Trim(Expression.Substring(p,i-p)));
    // remove spaces
    for i := List.Count-1 downto 0 do
    begin
      if (List[i].IsEmpty) then
        List.Delete(i);
    end;
    // correction for unary -
    for i:=0 to List.Count-1 do
    begin
      if List[i]='-' then
      begin
        if (i=0) then
          List[i]:='_'
        else if (List[i-1]='(') then
          List[i]:='_'
      end;
    end;

    SrcQueue.Clear;
    for i := 0 to List.Count-1 do
    begin
      SrcQueue.AddNew(List[i]);
    end;
  finally
    List.Free;
  end;

end;

procedure TVK_ExpressionParser.CheckBrackets();
begin

end;

//function TVK_ExpressionParser.FindSubExpression(var ind1, ind2: integer):Boolean;
//var
//  i, p: integer;
//begin
//  result := false;
//  p := FList.IndexOf(')');
//  if p<>-1 then
//  begin
//    i:=p-1;
//    while i>=0 do
//    begin
//      if FList[i]='(' then
//      begin
//        ind1:=i;
//        ind2:=p;
//        if ind2-ind1<2 then
//          raise Exception.CreateFmt('Empty brackets at position %d',[i]);
//        result := True;
//        exit;
//      end;
//      Dec(i);
//    end;
//    if (i<0) then
//      raise Exception.CreateFmt('Left bracket not found from position %d',[p]);
//  end;
//end;

function TVK_ExpressionParser.ToString(Q: TVK_EP_Elements):string;
var
  i: Integer;
  a:TArray<TVK_EP_Element>;
begin
  result :='';
  a:=q.ToArray;
  for i := 0 to High(a) do
  begin
    result := result + a[i].text + ' ';
  end;
end;

function TVK_ExpressionParser.ToString(q: TStack<TVK_EP_Element>):string;
var
  i:integer;
  a:TArray<TVK_EP_Element>;
begin
  result :='';
  a:=q.ToArray;
  for i := 0 to High(a) do
  begin
    result := result + a[i].text +' ';
  end;
end;

procedure TVK_ExpressionParser.ConvertToRPN();
var
  op,tmp:TVK_EP_Element;
//  s:string;
  OpStack:TStack<TVK_EP_Element> ;
begin
  ResultList.Clear;
  OpStack:=TStack<TVK_EP_Element>.Create;
  try
//    s := tostring(SrcQueue);
//    s := ToString(OpStack);
    while SrcQueue.Count > 0 do
    begin
      op := SrcQueue.Extract(SrcQueue.First);
      case op.kind of
        etOperand:
          ResultList.Add(op);
        etOperator:
          begin
            repeat
              tmp := NIL;
              if OpStack.Count > 0 then
                tmp := OpStack.Peek;
              if (tmp <> nil) and (tmp.kind = etOperator) and (tmp.priority >= op.priority) then
              begin
                ResultList.Add(OpStack.Pop);
//                OpStack.Push(op);
              end
              else
              begin
                OpStack.Push(op);
                Break;
              end;
            until false;
          end;
        etOpenBracket:
          OpStack.Push(op);
        etCloseBracket:
          begin
            repeat
              tmp := OpStack.Pop;
              if tmp.kind = etOpenBracket then
              begin
                tmp.Free;
                Break;
              end
              else
                ResultList.Add(tmp);
            until false;
            op.Free;
          end;
      end;
    end;

    while OpStack.Count > 0 do
      ResultList.Add(OpStack.Pop);
  finally
    FreeAndNil(OpStack);
  end;
end;

procedure TVK_ExpressionParser.CheckSyntax();
begin
  CheckBrackets();
end;

function TVK_ExpressionParser.Parse: Boolean;
begin
  try
    Split();
    CheckSyntax();
    ConvertToRPN();
    result := True;
  except
    result := false;
  end;
end;

function TVK_ExpressionParser.GetValue(e:TVK_EP_Element):single;
begin
  if not e.isExpression then
    Result := e.value
  else if Assigned(FOnGetValue) then
    result := FOnGetValue(Self, e.text)
  else
    raise Exception.Create('Variable value is not defined. '+e.text);
end;

function TVK_ExpressionParser.Evaluate: Single;
var
  e: TVK_EP_Element;
  waitQ : TStack<TVK_EP_Element>;
  RPNList: TVK_EP_Elements;

  function CheckWaitOperands(num:integer):Boolean;
  begin
    result := waitQ.Count >= num;
  end;

  procedure ProcessUnaryOp();
  var
    e1, tmp: TVK_EP_Element;
    v1: Single;
  begin
    if not CheckWaitOperands(1) then
      raise Exception.Create('Operand not found for unary operation.');
    e1 := waitQ.Pop();
    v1 := GetValue(e1);
    result := -v1;
    tmp:=TVK_EP_Element.Create(result);
    waitQ.Push(tmp);
    e1.Free;
  end;

  procedure ProcessBinaryOp();
  var
    e1, e2, tmp: TVK_EP_Element;
    v1, v2: Single;
  begin
    if not CheckWaitOperands(2) then
      raise Exception.Create('Operand not found for operation '+e.text);
    e1 := waitQ.Pop;
    v1 := GetValue(e1);
    e2 := waitQ.Pop;
    v2 := GetValue(e2);
    if e.text='-' then
      Result := v2 - v1
    else if e.text = '+' then
      Result := v2 + v1
    else if e.text = '*' then
      Result := v2 * v1
    else if e.text = '/' then
      Result := v2 / v1;
    tmp:=TVK_EP_Element.Create(result);
    waitQ.Push(tmp);
    e1.Free;
    e2.Free;
  end;

begin
  result := 0;
  if ResultList.Count=0 then
    if not Parse() then
      exit;

  RPNList := TVK_EP_Elements.Clone(ResultList);
  waitQ := TStack<TVK_EP_Element>.Create;
  try
    while RPNList.Count>0 do
    begin
      e := RPNList.Extract(RPNList.First);
      case e.kind of
        etOperator:
        begin
          if e.text='_' then
            ProcessUnaryOp()
          else
            ProcessBinaryOp();
          e.Free;
        end;
        etOperand: begin
          waitQ.Push(e);
        end;
        etOpenBracket, etCloseBracket:
          raise Exception.Create('We can''t be here!');
      end;
    end;
    if waitQ.count>0 then
    begin
      e:=waitQ.Pop;
      result := GetValue(e);
      e.free;
    end;
  finally
    FreeAndNil(RPNList);
    while waitQ.Count>0 do
      waitQ.Pop.Free;
    FreeAndNil(waitQ);
  end;
end;

{ TVK_EP_Elements }

function TVK_EP_Elements.AddNew(const AText: string): TVK_EP_Element;
begin
  result := TVK_EP_Element.Create(AText);
  Add(Result);
end;

constructor TVK_EP_Elements.Clone(const E: TVK_EP_Elements);
var
  i: Integer;
begin
  inherited Create(True);
  for i := 0 to e.Count-1 do
    Add ( TVK_EP_Element.Clone(e[i]) );
end;

{ TVK_EP_Element }

procedure TVK_EP_Element.AssignFrom(const e: TVK_EP_Element);
begin
  text := e.text;
  value := e.value;
  kind := e.kind;
  priority := e.priority;
  isExpression := e.isExpression;
end;

procedure TVK_EP_Element.CheckExpression;
begin
  isExpression := not TryStrToFloat(text, value, localFS);
end;

constructor TVK_EP_Element.Create(const AText: string);
begin
  inherited Create;
  text := AText;
  if AText='(' then
  begin
    kind := etOpenBracket;
    priority := 1;
  end
  else if AText=')' then
  begin
    kind := etCloseBracket;
    priority := 1; // not needed actually
  end
  else if (AText='*')or(AText='/') then
  begin
    kind := etOperator;
    priority := 3;
  end
  else if (AText='+')or(AText='-') then
  begin
    kind := etOperator;
    priority := 2;
  end
  else if (AText='_') then
  begin
    kind := etOperator;
    priority := 4;
  end
  else
  begin
    kind := etOperand;
    CheckExpression();
  end;
end;

constructor TVK_EP_Element.Clone(const e: TVK_EP_Element);
begin
  inherited Create;
  AssignFrom(e);
end;

constructor TVK_EP_Element.Create(AValue: Single);
begin
  inherited Create;
  value := AValue;
  text := formatFloat('0.0000', value, localFS);
  kind := etOperand;
end;

destructor TVK_EP_Element.Destroy;
begin
  inherited Destroy;
end;

initialization
  localFS := TFormatSettings.Create();
  localFS.DecimalSeparator := '.';

finalization


end.
